//
//  ViewController.swift
//  AFSample
//
//  Created by Edy Cu Tjong on 6/10/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Alamofire.request("https://codewithchris.com/code/afsample.json").responseJSON { (response) in
            // Check if the result has a value
            if let json = response.result.value as? NSDictionary {
                print(json["firstkey"] as! String)
                print(json["secondkey"] as! NSArray)
            }
        }
    }


}

